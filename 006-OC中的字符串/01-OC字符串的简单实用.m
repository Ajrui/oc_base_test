
#import <Foundation/Foundation.h>
#import <stdio.h>

int main() {
    
//    char *s = __FILE__;
    
    // 如果C语言字符串中包含了中文,NSLog无法输出
//    NSLog(@"输出了一句话----------",__FILE__);
    
    printf("输出了-------%s-----%i----%s\n",__FILE__, __LINE__,__func__);
    // 输出了-------01-OC字符串的简单实用.m-----12----main
    return 0;
    
}

void test1() {
    
    // 快速创建OC字符串
    NSString *name = @"Jack";
    
    NSLog(@"我的名字叫做%@", name);
    
    
    int age = 10;
    
    // 产生一个新的字符串
    NSString *str = [NSString stringWithFormat:@"My age is %i", age];
    NSLog(@"str=%@",str);
}