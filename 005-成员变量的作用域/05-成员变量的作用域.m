
/*
 @public: 哪里都可以访问
 @protected: 当前类和子类的@implementation和@end之间
 @private : 当前类的@implementation和@end之间
 @package: 在某个"体系"里面能直接访问(在某个框架里面可以直接访问)
 
 作用域范围
 @public > @protected > @private
 
 如果在@interface中声明的成员变量没有说明作用域,那么就是@protected
 如果在@implementation中声明的成员变量没有说明作用域,那么就是@private
 
 */

#import <Foundation/Foundation.h>

@interface Person : NSObject {
    
    @protected
    int _age;
    @private
    int _height;
}

- (int) height;

- (void) study;

@end

@implementation Person {
     int _weight; // 子类用不了
}

- (void) study {
    NSLog(@"%i岁的人在学习~~~~~~",_age);
}

- (int) height {
    
    return _height;
}

@end

@interface Student : Person

@end

@implementation Student

- (void) study {
    NSLog(@"%i岁  %i身高  的人在学习!!!! ", _age, [super height] );
}

@end



int main() {
    
    Person *p = [Person new];
//    p->_age = 100;
    [p study];
    
//    NSLog(@"age=%i",p->_age);
    Student *s = [Student new];
    [s study];
    
    
    return 0;
}