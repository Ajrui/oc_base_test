//
//  Person.h
//  01-分类-category
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

/*
  // 分类
 
  // 声明
     @interface Person(分类名称)
    
     @end
 
  // 实现
    @implementtation Person(分类名称)
 
    @end
 
 
 */

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, assign) int age;

@end
