//
//  NSString+Number.h
//  01-分类-category
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Number)

// 计算字符串中阿拉伯数字的个数
- (int) numberCount;

@end
