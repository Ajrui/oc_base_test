//
//  main.m
//  02-分类-category
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Person+Jason.h"
#import "NSString+Number.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        /*
        Person *p = [[Person alloc] init];
        
        [p study];
        
        [p release];
        */
        
        // 给NSString写的一个分类 添加一个可以查询数字个数的方法
        NSString *str = @"abc 123asdasdasdasd";
        
        int count = [str numberCount];
        
        NSLog(@"count = %d",count);
        
    }
    return 0;
}
