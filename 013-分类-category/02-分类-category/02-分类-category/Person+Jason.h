//
//  Person+Jason.h
//  02-分类-category
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Person.h"

@interface Person (Jason)

// 声明
- (void) study;

@end
