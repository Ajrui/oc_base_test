//
//  NSString+Number.m
//  02-分类-category
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "NSString+Number.h"

@implementation NSString (Number)


- (int)numberCount {
    
//    [self length];
    int len = (int)self.length;
    int count = 0;
    for (int i = 0; i < len; i++) {
        // 获取i位置对应的字符(char)
        char c = [self characterAtIndex:i];
        
        if(c >='0' && c<='9') {
            
            count++;
        }
    }
    
    return count;
}
@end
