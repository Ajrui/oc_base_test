//
//  Person.m
//  03-内存管理-对象之间的内存
//
//  Created by 何志勇 on 15/9/11.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

/*
 成员变量的内存管理(如果成员变量是对象,才需要管理)
 1.set方法的管理
    1> release旧的成员变量,retain新的成员变量
    2>
 
 2.delloc方法的管理
    1> release成员变量
 */



#import "Person.h"

@implementation Person

- (void)setDog:(Dog *)dog {
    
    if (dog != _dog) {
        [_dog release];
        _dog = [dog retain];
    }
}

- (Dog *)dog {
    
    return _dog;
}

- (void)setCard:(Card *)card {
    
    if(_card != card) {
        [_card release];
        _card = [card retain];
    }
}

- (Card *)card {
    
    return _card;
}

- (void) dealloc {
    [_card release];
    [_dog release];
    NSLog(@"人被释放掉");
    [super dealloc];
}
@end
