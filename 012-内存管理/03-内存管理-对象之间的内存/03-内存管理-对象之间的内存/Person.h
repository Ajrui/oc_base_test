//
//  Person.h
//  03-内存管理-对象之间的内存
//
//  Created by 何志勇 on 15/9/11.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dog.h"
#import "Card.h"

@interface Person : NSObject {
    
    Dog *_dog;
    
    Card *_card;
}

- (void) setDog:(Dog *)dog;
- (Dog *) dog;

- (void) setCard:(Card *)card;
- (Card *)card;

@end
