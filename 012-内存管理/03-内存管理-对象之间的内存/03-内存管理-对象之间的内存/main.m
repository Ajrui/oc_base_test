//
//  main.m
//  03-内存管理-对象之间的内存
//
//  Created by 何志勇 on 15/9/11.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Person *p = [[Person alloc] init];
        
        Dog *dog = [[Dog alloc] init];
    
        p.dog = dog;
        
        [dog release];
        
        // card 1
        Card *card = [[Card alloc] init];
        // card 2
        p.card = card;
        // card 1
        [card release];
        
        
        // card 0
        [p release];
        
    }
    return 0;
}
