//
//  Person.m
//  02-内存管理
//
//  Created by 何志勇 on 15/9/10.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Person.h"

@implementation Person


- (void)setBook:(Book *)book {
    
    if(book != _book) {
        
        // +1
        // 先对旧的书本做一次release
        [_book release];
        _book = [book retain];
        
    }
    
}

- (Book *)book {
    
    return _book;
}

- (void) dealloc {
    
    // -1
    [_book  release];
    NSLog(@"Person被回收");
    
    // super的方法放到最后
    [super dealloc];
    
    
}


@end
