//
//  Person.h
//  02-内存管理
//
//  Created by 何志勇 on 15/9/10.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Book.h"

@interface Person : NSObject {
    
    Book *_book;
}

@property int age;

- (void) setBook:(Book *) book;

- (Book *) book;

@end
