//
//  main.m
//  02-内存管理
//
//  Created by 何志勇 on 15/9/10.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Person.h"

// 创建一本新书
void createBook(Person *person) {
    
    // book:1
    Book *book = [[Book alloc] init];
    book.price = 10;
    // book:2
    person.book = book;
    // book:1
    [book release];
    
    // book2 : 1
    Book *book2 = [[Book alloc] init];
    book2.price = 20;
    // book2:2
    // book : 0
    person.book = book2;
    // book2: 1
    [book2 release];
    
    person.book = book2;
    person.book = book2;
    person.book = book2;
    person.book = book2;
    person.book = book2;
    
}

// 读取一本书
void readBook(Person *person) {
    
    int price = person.book.price;
    NSLog(@"这本书的价格是%d",price);
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Person:1
        Person *p =  [[Person alloc] init];
        p.age = 10;
        
        // 创建新书
        // book : 0
        // book2: 1
        createBook(p);
        
        // 读书
        readBook(p);
        readBook(p);
        readBook(p);
        readBook(p);
        readBook(p);
        
        // Person : 0
        // book2 : 0
        [p release];
    
    
    }
    return 0;
}
