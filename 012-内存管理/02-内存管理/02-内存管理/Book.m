//
//  Book.m
//  02-内存管理
//
//  Created by 何志勇 on 15/9/10.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Book.h"

@implementation Book

- (void) dealloc {
    
    NSLog(@"Book被回收");
    
    // super的方法放到最后
    [super dealloc];
    
    
}

@end
