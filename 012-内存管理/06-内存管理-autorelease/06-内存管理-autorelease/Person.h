//
//  Person.h
//  06-内存管理-autorelease
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, assign) int age;

+ (Person *) person;
@end
