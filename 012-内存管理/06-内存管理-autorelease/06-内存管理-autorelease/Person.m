//
//  Person.m
//  06-内存管理-autorelease
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Person.h"

@implementation Person

+ (Person *)person {
    
    Person *p = [[[Person alloc] init ] autorelease];
    
    return p;
    
}

- (void)dealloc {
    
    NSLog(@"Person被销毁了");
    [super dealloc];
}

@end
