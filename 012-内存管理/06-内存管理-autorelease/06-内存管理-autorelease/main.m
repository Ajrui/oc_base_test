//
//  main.m
//  06-内存管理-autorelease
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

/*
 
 苹果官方提供的方法(API)
 1.如果方法名不是alloc、 new,就不用release或者autorelease
 2.如果方法名是alloc、new,就必须release或者autorelease
 
 */

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    
    @autoreleasepool {
        
        Person *p = [Person person];
        p.age = 20;
        
        
        NSString *str= [NSString stringWithFormat:@"123"];
        
        NSString *str2 = [[[NSString alloc] initWithFormat:@"123"] autorelease];
        
        [str2 release];
        
    }
    
      return 0;
}


void test() {
    
    @autoreleasepool {// 从第13行代码开始,在内存创建了一个自动释放池
        
        // autorelease 返回的对象本身
        // autorelease 会将对象加到池子里去,计数器没有变
        Person *p = [[[Person alloc] init] autorelease];
        
        p.age = 20;
        
        
    } // 括号过后,池子销毁,池子里所有的对象都会做一次release操作
    
    
    @autoreleasepool {
        
        Person *p2 = [[[Person alloc] init] autorelease];
        
        
    }
    
    /*
     ios 5.0之前的写法
     NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
     
     [pool release];
     
     */

}
