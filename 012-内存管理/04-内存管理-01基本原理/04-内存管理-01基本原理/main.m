//
//  main.m
//  04-内存管理-01基本原理
//
//  Created by Jason on 15/9/12.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

/*
 原则:
    1.只要你调用了alloc、new创建一个新对象,那么你就有责任做2次release
    2.只要你调用了retain,那么你也有责任做一次release
    3.不能再操作已经被释放的对象,不然会发生野指针错误
    4.在对象释放之前,才能操作对象
 有加就应该有减
 
 */

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        // 1
        Person *p = [[Person alloc] init];
        
        // 让对象的计数器+1, 而且会返回对象本身  计算器占4个字节
        // 2
        [p retain];
        
        // 1
        [p release];
        
        // 0
        [p release];
    
        // message sent to deallocated instance 0x100111e80
        // 野指针错误
        [p setAge:20];
    
    }
    return 0;
}
