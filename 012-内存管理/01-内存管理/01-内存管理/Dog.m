//
//  Dog.m
//  01-内存管理
//
//  Created by 何志勇 on 15/9/9.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Dog.h"

@implementation Dog

#pragma mark 当一个对象从内存中移除的时候就会调用
- (void) dealloc {
    
    NSLog(@"Dog被销毁了--------");
    // 一定要调用super的方法
    [super dealloc];
}

@end
