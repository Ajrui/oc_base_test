//
//  main.m
//  01-内存管理
//
//  Created by 何志勇 on 15/9/9.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

/*
 retain: 计数器 +1
 release: 计算器 -1,计数器减为0时,对象才会被回收,而且系统会自动调用对象的dealloc方法
 retainCount: 返回对象当前的计数器
 
 */

#import <Foundation/Foundation.h>
#import "Dog.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 1
        Dog *dog = [[Dog alloc] init];
        
        /*
        [dog release];
        
                
        dog = [[Dog alloc] init];
        
        [dog release]; // 从这行代码过后, dog变成了野指针 指向了不可用的内存
        
        // nil = 0;
        dog = nil; // dog变成了空指针
        
        [dog release];
        
        [dog release];
         
         */
        
        // 2
        [dog retain];
        // 3
        [dog retain];
        
        int count = (int)[dog retainCount];
        
        NSLog(@"count=%d",count);
        
        // 2
        [dog release];
        
        // 1
        [dog release];
        
        // 0
        [dog release];
//        NSLog(@"Hello, World!");
    }
    
    return 0;
}
