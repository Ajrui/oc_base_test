//
//  Person.m
//  05-内存管理-property参数
//
//  Created by jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Person.h"

@implementation Person

/*
- (Cat *)cat {
    
    return _cat;
}


- (void)setCat:(Cat *)cat {
    
    if(_cat != cat) {
        
        [_cat release];
        _cat = [cat retain];
        
    }
}


- (Pig *)pig {
    
    return _pig;
}

- (void) setPig:(Pig *)pig {
    
    if(_pig != pig) {
        
        [_pig release];
        _pig = [pig retain];
    }
}

*/

- (void)dealloc {
    
    [_cat release];
    [_pig release];
    [_house release];
    [super dealloc];
}

@end
