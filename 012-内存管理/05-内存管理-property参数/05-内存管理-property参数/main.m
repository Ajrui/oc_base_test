//
//  main.m
//  05-内存管理-property参数
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Person *p = [[Person alloc] init];
        
        Cat *c = [[Cat alloc] init];
        
        p.cat = c;
        [c release];
        
        [p release];
        
    }
    return 0;
}
