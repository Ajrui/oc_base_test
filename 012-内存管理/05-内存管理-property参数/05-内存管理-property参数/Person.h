//
//  Person.h
//  05-内存管理-property参数
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cat.h"
#import "Pig.h"
#import "House.h"

@interface Person : NSObject {
    
    Cat *_cat;
    Pig *_pig;
    House *_house;
}

//- (void) setCat:(Cat *)cat;
//- (Cat *) cat;

// 这里的retain: 生成set方法实现中, release旧值,retain新值
@property (retain) Cat *cat; // 添加参数 retain会自动生成set方法中的retain

//- (void) setPig:(Pig *)pig;
//- (Pig *) pig;

@property (retain) Pig *pig;

@property (retain) House *house;

@end
