//
//  Student.h
//  05-内存管理-property参数
//
//  Created by Jason on 15/9/13.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "House.h"

@interface Student : NSObject

/*
 @property的参数:
 一、控制set方法的内存管理:
    1.retain: release旧值, retain新值
    2.assign: 直接赋值,不做任何内存管理(默认)
    3.copy:
 
 二、控制有没有set方法和get方法
    1.readwrite: 同时生成set方法和get方法(默认, 用的少)
    2.readonly:  只会生成get方法
 
 三、多线程管理
    1.atomic    : 性能低(默认)
    2.nonatomic : 性能高
 
 四、控制set方法和get方法的名称
    1.setter : 设置set方法的名称,一定要加冒号:
    2.getter : 设置get方法的名称
 */

@property (nonatomic, assign,readwrite) int age;

@property (nonatomic, retain) House *house;

@property (nonatomic, assign, getter=getHeight,setter=setHeight:) int height;


@end
