
#import <Foundation/Foundation.h>

/*
 多态:多种形态
 多态在代码中的体现:父类指针 指向 子类对象
 多态的条件:必须有继承
 
 每一个OC对象都有多态性
 
 多态的局限性: 父类指针 不能 直接调用子类特有的方法,如果要正常调用子类特有的方法,应该将父类指针强制转换为子类指针
 
 */

@interface Zoombie : NSObject
- (void) walk;
@end

@implementation Zoombie

- (void) walk {
    NSLog(@"僵尸走几步....");
}
@end


// 跳跃僵尸
@interface JumpZoombie : Zoombie
- (void)jump;
@end

@implementation JumpZoombie

- (void) jump {
    NSLog(@"跳跳跳");
}
- (void) walk {
    NSLog(@"僵尸跳几步....");
}

@end


// 跳舞僵尸
@interface DancerZoombie : Zoombie

@end

@implementation DancerZoombie

- (void) walk {
    
    NSLog(@"僵尸摇几步....");
}
@end




void letZoombieWalk(Zoombie *z) {
    
    [z walk];
//    [z jump];
    // 强制转换
    JumpZoombie *j = (JumpZoombie *)z;
    [j jump];
}


int main() {
    // 第一种形态
    /*
    JumpZoombie *jump = [JumpZoombie new];
    
    [jump walk];
    
     */
    
    // 父类指针指向子类对象
    // Zoombie类型的指针指向了新建的对象
    // 第二种形态
    Zoombie *jump = [JumpZoombie new];
    // 这个方法执行过程中会 做一个 “动态绑定”
    [jump walk];
    
    letZoombieWalk([JumpZoombie new]);
    
    DancerZoombie *dance = [DancerZoombie new];

//    letZoombieWalk(dance);
    
    
    JumpZoombie *jump1 = [JumpZoombie new];
    letZoombieWalk(jump1);
    
    return 0;
}



