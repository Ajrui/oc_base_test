//
//  Card.m
//  01-id和@class
//
//  Created by Jason on 15/9/20.
//  Copyright © 2015年 Jason. All rights reserved.
//

#import "Card.h"
#import "Person.h"

@implementation Card

- (void)dealloc {
    
    NSLog(@"card被释放掉了");
//    [_person release]; 因为声明文件中使用的是assign 这里无需释放
    [super dealloc];
}

@end
