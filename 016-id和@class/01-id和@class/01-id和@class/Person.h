//
//  Person.h
//  01-id和@class
//
//  Created by Jason on 15/9/20.
//  Copyright © 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Dog.h" 没有@class高效 因为导入头文件会将所有内容读取进来
//#import "Card.h" 与Card不能互相嵌套import,@class可以解决这个问题
@class Card;

// 最简单、效率最高的类声明方法
@class Dog;

@interface Person : NSObject

- (void) test;

@property (nonatomic, retain) Dog *dog;

@property (nonatomic, retain) Card *card;

@end
