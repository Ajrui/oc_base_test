//
//  Card.h
//  01-id和@class
//
//  Created by Jason on 15/9/20.
//  Copyright © 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Person.h"  与person不能互相嵌套的improt 而@class解决了这个问题
@class Person;
@interface Card : NSObject

@property (nonatomic, assign) Person *person;

@end
