//
//  main.m
//  01-id和@class
//
//  Created by Jason on 15/9/20.
//  Copyright © 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Card.h"

/*
 id总结:
    id 类型的指针变量可以指向任何对象且变量名不用写*
 
 @class总结:
 
    1.在声明文件中直接使用@class比import "xxx.h"效率高
        因为import "xxx.h"是拷贝文件会导入文件中所有的方法 而@class只是声明个类而已
    2.在实现文件中使用import "xxx.h",什么时候用到什么时候导入
    3.解决两个类互相嵌套import编译报错的问题
 
 
 @class 和 #import的区别:
 
    1.@class做一个简单的声明,并不包含类的方法声明、成员变量声明.
    #import纯粹是把整个类的所有声明都拷贝一份
 
    2.#import不能嵌套包含,但是@class能实现互相引用的问题
 
 */

int main(int argc, const char * argv[]) {
    // 循环retain问题  类似java死锁 谁也不释放谁
    /*
       解决办法:
        一端用retain,另一端用assign
     */
    @autoreleasepool {
        // p 1
        Person *p = [[Person alloc] init];
        // c 1
        Card *c = [[Card alloc] init];
        
        // c 2
        p.card = c;
        // p 1
        c.person = p;
        // p 0
        // c 1
        [p release];
        // c 0
        [c release];
        
    }
    return 0;
}


// id 使用注意事项
void test1() {
    // id 类型的指针变量可以指向任何对象且变量名不用写*
    
    id abc = @"123";
    
    [abc length];
    
    //        id p = [[[Person alloc] init] autorelease]; 不建议这么写
    Person *p = [[[Person alloc] init] autorelease];
    
    [p test];
    
}
