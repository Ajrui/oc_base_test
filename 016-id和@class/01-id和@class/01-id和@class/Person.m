//
//  Person.m
//  01-id和@class
//
//  Created by Jason on 15/9/20.
//  Copyright © 2015年 Jason. All rights reserved.
//

#import "Person.h"
#import "Dog.h" // 声明文件用@class 实现方法文件中用import头文件
#import "Card.h"

@implementation Person

- (void)test {
    
    [_dog brak];
    NSLog(@"test被调用了");
}

- (void)dealloc {
   
    NSLog(@"person被释放掉了");
    [_dog release];
    [_card release];
    [super dealloc];
}
@end
