#import <Foundation/Foundation.h>

@interface TestClass : NSObject

/*
- (void) objMethod1;

+ (void) classMethod1;
 */

+ (void) test;

@end

@implementation TestClass


+ (void) test
{
    NSLog(@"test-----------");
    // 会引发死循环
    [self test];
}

/*
 下面的self使用都不正确
- (void) objMethod1
{
    NSLog(@"调用了objMethod1方法----对象方法");
    // 编译器会将classMethod1当做是对象方法处理
    [self classMethod1];
}

+ (void) classMethod1
{
    // 编译器会将objMethod1当做是类方法处理
    [self objMethod1];
}
 */

@end


int main()
{
    [TestClass test];
    
    return 0;
}
