
/*
 对象方法:
    1>以减号-开头
    2>只能让对象调用,没有对象,这个方法根本不可能被执行
    3>对象方法能访问实例变量(成员变量)
 
 类方法:
    1>以加号+开头
    2>只能用类名调用,对象不能调用
    3>类方法中不能访问实例变量(成员变量)
    4>使用场合: 当不需要对象而且不需要访问成员变量的时候,尽量用类方法
 
 
 类方法和对象方法可以同名
 
 */

#import <Foundation/Foundation.h>

@interface Person : NSObject {
    
    int _age;
}

- (void) setAge : (int)age;

- (int) age;

// 方法声明
- (void) study;

// 类方法: +开头
+ (void)printClassName;

// 对象方法: +开头
- (void)printClassName;


@end


@implementation Person

- (void) setAge : (int) age {
    
    if(age <= 0) {
        age = 1;
    }
    
    _age = age;
}

- (int) age {
    
    return _age;
}

- (void) study {
    
    NSLog(@"%i岁的人在学习", _age);
}

// 类方法的实现
+ (void)printClassName {
    
    // instance variable '_age' accessed in class method
    // 实例变量_age不能在类方法中访问
    //    _age = 10;
    
    NSLog(@"这个类叫做Person");
}

// 对象方法的实现
- (void)printClassName {
    
    // instance variable '_age' accessed in class method
    // 实例变量_age不能在类方法中访问
    //    _age = 10;
    
    NSLog(@"这个类叫做Person");
}

@end


int main() {
    
    [Person printClassName];
    
    
    // new 是一个类方法
    Person *p = [Person new];
    [p setAge:20];
    [p study];
    
     // 下面的调用是错误的,系统会将printClassName当做是对象方法来处理
     //[p printClassName]; // 如果声明了对象方法 则不会报错
    

    
    return 0;
}






