/*
 设计一个计算器类
    1.类名: Cacultor
    2.行为(方法):
        *返回PI:3.14
        *计算某个数值的平方
        *计算两个数值的和
 */
#import <Foundation/Foundation.h>

// 计算器的声明
@interface Caculator : NSObject {
    
}

// 方法声明
- (double)pi; // 方法名pi

- (double)pi:(int)bac; // 方法名pi:  与上面方法名是不一样的 这不是重载

// 一个参数对应一个冒号:
// 冒号也是方法名的一部分
- (double)pingfang:(double)number;


// 两个参数 方法名:sumOfNum1:andNum2;
- (double)sumOfNum1:(double)num1 andNum2:(double)num2;



@end

int main() {
    
    Caculator *ca = [Caculator new];
    
    double p = [ca pi];
//    NSLog(@"p=%f", p);
    
    /*
     
     接收一个参数的类型
     double d = [ca pingfang:4];
     NSLog(@"4的平方=%f", d);
    
     */
    
    double sum = [ca sumOfNum1:10 andNum2:5];
    NSLog(@"10+5=%f", sum);
    
    
    return 0;
}

// 计算器的实现
@implementation Caculator

- (double)pi {
    
    return 3.14;
}

- (double)pi:(int)abc {
    
    return abc + abc
}


- (double)pingfang:(double)number {
    
    return number * number;
}

- (double) sumOfNum1:(double)num1 andNum2:(double)num2 {
    
    return num1 + num2;
}


@end


