#import <Foundation/Foundation.h>


@implementation Car : NSObject
{
    @public
    int wheels;
    int speed;
}

- (void) run
{
    NSLog(@"%i个轮子、速度%i的车子跑起来了！", wheels, speed);
}

@end


int main()
{
    
    Car *c = [Car new];
    c->wheels = 4;
    c->speed = 120;
    
    Car *c2 = c;
    c2->speed = 100;
    
    // 给指针变量C指向的对象发送一条run消息
    // 接收消息的那个对象,成为"消息接收者"
    [c run]; // 4 100
    
    return 0;
}