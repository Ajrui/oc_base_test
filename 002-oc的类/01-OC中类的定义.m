// 为了能使用NSObject
#import <Foundation/Foundation.h>
// 设计一个车类
// @implementation 和 @end
// : NSObject：让Car这个类具备创建对象的能力
@implementation Car : NSObject {
    // 这个大括号里面只能写所有的属性
    
    // @public:让对象的属性可以被外面的指针访问
    @public
    int wheels; // 轮子个数
    double speed; // 时速
}

// 在@end的前面, 大括号{}外面写行为
// 给Car对象增加一个行为(方法)
// 给对象增加一个行为,必须以减号 - 开头
// OC方法中的小括号()只是主要扩住类型
- (void) run 
{
    
    // 访问车子对象内部的属性,直接用属性名就可以
    NSLog(@"%i个轮子,%f时速的车子跑起来了!",wheels,speed);
}


@end

int main() {
    
    // 在OC中想执行一些行为,首页要写个[行为执行者 行为名称]
    // 利用Car这个类,执行创建车子(new) 行为
    // new这个行为执行完毕后,会返回这个对象的地址
    // 定义了一个指向Car类型数据的指针变量c
    // 指针变量c指向的是最新创建的车子对象
    
    Car *c = [Car new];
    // 给c指向的车子对象的wheels属性赋值
    c->wheels = 4;
    
    c->speed = 300;
    
//    NSLog(@"车有%i个轮子,时速%f km/h", c->wheels, c->speed);
    
    //  让内存的车子对象执行跑(run)这个行为
    //  给指针变量c指向的对象发送一条run消息,让这个对象执行run这个行为
    [c run];
    
    [c run];
    
    
    
    return 0;
    
}


