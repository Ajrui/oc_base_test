#import <Foundation/Foundation.h>

// 声明
@interface Car : NSObject {
    
    @public
    int speed; // 速度
}

- (int)compareSpeedWithOther:(Car *)other;

@end

// 实现
@implementation Car
/*
{   // 实现和声明中的成员变量不能同名
    int speed;
}
 
 */

- (int)compareSpeedWithOther:(Car *)other {
    // 说明本车速度快
    if(speed > other->speed) return 1;
    
    // 说明本车速度慢
    if(speed < other->speed) return -1;
    
    // 说明车速相同
    return 0;
}

@end



int main() {
    
    Car *car1 = [Car new];
    car1->speed = 100;
    
    Car *car2 = [Car new];
    car2->speed = 200;
    
    int result = [car1 compareSpeedWithOther : car2];
    
    NSLog(@"result= %i",result);
    
    return 0;
    
}






