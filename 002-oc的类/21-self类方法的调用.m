
/*
 self
    1.出现的地方:所有的OC方法中(对象方法\类方法),不能出现在函数
    2.作用
        1>使用 "self->成员变量名" 访问当前方法调用的成员变量
        2>使用 "[self 方法名];" 来调用方法(对象方法\类方法)
 */

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

+ (double) sumOfNum1:(double) num1 andNum2:(double) num2;

+ (double) averageOfNum1:(double) num1 andNum2:(double) num2;


@end

@implementation Calculator

+ (double) sumOfNum1:(double) num1 andNum2:(double) num2 {
    
    return num1 + num2;
}

+ (double) averageOfNum1:(double)num1 andNum2:(double) num2 {

    
//    return (num1 + num2) / 2;
    
    // self 指向方法调用者
    // 在类方法中,self一定指向类
    
    return [self sumOfNum1:num1 andNum2: num2] / 2;
    
}

@end


int main() {
    
    double d = [Calculator sumOfNum1:10 andNum2 : 10];
    
    double result = [Calculator averageOfNum1:10 andNum2 : 12];
    
    NSLog(@"d is %f", d);
    NSLog(@"result is %f", result);
    
    return 0;
}