#import <Foundation/Foundation.h>

/*
 设计一个类:
 1.声明类:@interface和@end
    *类名
    *继承了NSObject
    *属性
    *方法(行为,只需要声明)
 2.实现(定义)类: @implementation和@end
    *实现@interface中声明的方法
*/

@interface Dog : NSObject { // 类的声明
    // 属性
    @public
    int speed;
    
}

// 只是声明
- (void) run;

@end


// 类的实现(定义)
@implementation Dog

- (void)run {
    NSLog(@"速度为%i的狗跑起来了",speed);
}

@end


int main(){
    
    Dog *d = [Dog new];
    d->speed = 100;
    
    [d run];
    
    return 0;
    
    
}