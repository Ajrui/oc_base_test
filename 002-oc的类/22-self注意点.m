#import <Foundation/Foundation.h>

@interface TestClass : NSObject

- (void) objMethod1;

+ (void) classMethod1;

@end


@implementation TestClass

- (void) objMethod1 {
//    NSLog(@"调用了objMethod1方法------对象方法");
    [self classMethod1];
}

+ (void) classMethod1 {
    
//    [self objMethod1]; 编译器会将objMethod1当做是类方法处理
    NSLog(@"调用了classMethod1方法------对象方法");
}

@end


int main() {
    
    [TestClass classMethod1];
    
    return 0;
}




