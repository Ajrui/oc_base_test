
#import <Foundation/Foundation.h>

@interface Zoombie : NSObject {
    
    int life;
    int speed;
}

// life的set方法:没有返回值,有参数
- (void) setLife:(int)newLife;
// life的get方法:没有参数,有返回值
- (int) life;

- (void) setSpeed:(int)newSpeed;

- (int) speed;


@end


@implementation Zoombie

- (void) setLife:(int) newLife {
    
    life = newLife;
    
}

- (int) life {
    
    return life;
}

- (void) setSpeed:(int) newSpeed {
    
    speed = newSpeed;
}

- (int) speed {
    
    return speed;
}

@end



int main() {
    
    Zoombie *z = [Zoombie new];
    // 通过set方法设置僵尸对象的life成员变量
    [z setLife:10];
    [z setSpeed:200];
    
    // 通过get方法获得僵尸对象的life成员变量
    int speed = [z speed];
    int life = [z life];
    
    NSLog(@"僵尸的生命值%i",life);
    NSLog(@"僵尸的速度值%i",speed);
    
    
    
    
    
    return 0;
}

