
#import <Foundation/Foundation.h>

/*
 成员变量的命名的规范
 */

@interface Person : NSObject {
    // 成员变量都以下划线 _ 开头
    // 1.可以跟get方法的名称区分开
    // 2.一看到下划线开头的变量,肯定是成员变量
    
    int _age;
}

- (void) setAge:(int)newAge;

- (int) age;

@end


// 实现

@implementation Person

- (void) setAge:(int)newAge {
    
    _age = newAge;
}

- (int) age {
    
    return _age;
}

@end


int main() {
    
    Person *p = [Person new];
    [p setAge : 20];
    
    int age2 = [p age];
    
    NSLog(@"年龄是%i",age2);
    
    
    return 0;
}