#import <Foundation/Foundation.h>


// 定义狗类
@implementation Dog : NSObject
// 定义狗的属性  默认都是0
{
    @public // public后面所有的属性都是公共的
    int speed;  // 速度
    int weight; // 体重
    
}

// 狗叫
- (void) bark
{
    NSLog(@"体重%i的狗叫了!",weight);
}

// 跑
- (void) run
{
    NSLog(@"体重%i的狗跑起来了!",speed);

}


@end




int main()
{
    // 利用Dog这个类创建Dog对象
    Dog *dog = [Dog new];
    dog->speed = 100;
    dog->weight = 30;
//    [dog run];
//    [dog bark];
    
    Dog *dog2 = [Dog new];
    dog2->speed = 90;
    dog2->weight = 40;
    [dog2 run];
    
    
    
    return 0;
    
}