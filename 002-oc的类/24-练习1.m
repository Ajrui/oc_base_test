#import <Foundation/Foundation.h>

@interface Point2D : NSObject {

    int _x;
    int _y;

}

- (void) setX:(int)x;
- (void) setY:(int)y;

- (int)x;
- (int)y;

// 设计一个对象方法来计算和其他点的距离
- (double) distanceWithOther:(Point2D *)other;

// 设计一个类方法 计算 两个点的距离
+ (double) distanceFrom:(Point2D *)from to:(Point2D *)to;


@end

@implementation Point2D

- (void) setX:(int) x {
    
    _x = x;
}

- (void) setY:(int) y{
    
    _y = y;
}

- (int) x {
    
    return _x;
}

- (int) y {
    
    return _y;
}

- (double) distanceWithOther:(Point2D *)other {
    
//    ([other y] - _y)的平方 + ([other x] - _x)的平方 伪代码
    return [Point2D distanceFrom:self to:other];
}

+ (double) distanceFrom:(Point2D *)from to:(Point2D *)to {
    
//    [from y] - [to y];
//    [from x] - [to x];
}

@end


int main() {
    
    Point2D *p1 = [Point2D new];
    [p1 setX:10];
    [p1 setY:10];
    
    
    Point2D *p2 = [Point2D new];
    [p2 setX:10];
    [p2 setY:10];
    
    return 0;
}








