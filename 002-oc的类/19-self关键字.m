/*
 self
 1.使用场合: 只能用在方法(对象方法\类方法),不能用在函数里面
 2.每次调用方法的时候,系统会自动创建self指针
 3.self指针指向方法调用者
 */

#import <Foundation/Foundation.h>

@interface Car : NSObject {
    
    int _wheels;
}

- (void) setWheels:(int)wheels;

- (int) wheels;

- (void) run;

@end

@implementation Car

// 所有的成员变量(不管是不是@public),都能直接在对象方法中访问
- (void) setWheels:(int)wheels {
    
    _wheels = wheels;
}

- (int) wheels {
    
    return _wheels;
}

- (void) run {
    
    // self是一个特殊的指针,只在@implementation的方法中才有
    // self其实是方法中一个内置指针, 每次调用方法,都会有self这个指针
    // self指针指向方法调用者
    
    int _wheels = 10;
    
    NSLog(@"%i个轮子的车子飞奔起来了!", self->_wheels);
}

/*
 错误写法:self 不能用在函数中
void test() {
    
    self->wheels = 10;
}
 */

@end


int main() {
    
    Car *c = [Car new];
    [c setWheels:4];
    
    [c run];
    
    
    return 0;
}


