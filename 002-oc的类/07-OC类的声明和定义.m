#import <Foundation/Foundation.h>
// 声明一个类
/*
 1.类名
 2.继承了NSObject
 3.声明属性
 4.声明方法(仅仅是声明,不需要实现)
 */
@interface Book : NSObject {
    @public
    double price; // 价格
}

// 声明一个方法
- (void) reng;

@end

int main() {
    
    Book *book = [Book new];
    book->price = 10;
    
    [book reng];
    
    return 0;
    
}

// 定义(实现)一个类
/*
 只用来实现@interface中声明的方法
 */
@implementation Book

- (void) reng {
    
    NSLog(@"%f书被扔了",price);
}

@end

