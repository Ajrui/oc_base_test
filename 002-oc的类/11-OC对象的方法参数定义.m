#import <Foundation/Foundation.h>


@interface Caculator : NSObject {
    
}

// 方法声明

- (double)pi;

- (double)pingfang:(double) number;

- (double) sumOfNum1:(double)num1 andNum2:(double)num2;

@end




int main() {
    
    Caculator *c = [Caculator new];
    
    double pi = [c pi];
    
    double pingfang = [c pingfang:4];
    
    double sum = [c sumOfNum1:4 andNum2:4];
    
    NSLog(@"pi的值是%f",pi);
    NSLog(@"pingfang的值是%f",pingfang);
    NSLog(@"sum的值是%f",sum);
    
    return 0;
}

@implementation Caculator

- (double) pi {
    
    return 3.14;
}

- (double) pingfang:(double) number {
    
    return number * number;
}

- (double)sumOfNum1:(double)num1 andNum2:(double)num2 {
    
    return num1 + num2;
    
}

@end

