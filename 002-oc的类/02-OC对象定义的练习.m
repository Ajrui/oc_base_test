#import <Foundation/Foundation.h>


@implementation Car : NSObject
{
    @public
    int wheels;
    double speed;
    
}

- (void) run
{
    NSLog(@"%i个轮子,%f的时速的车子开了", wheels,speed);
}


@end



int main()
{
    
    Car *c = [Car  new];
    c->speed = 120.80;
    c->wheels = 4;
    
    [c run];
    
    
    return 0;
    
}



