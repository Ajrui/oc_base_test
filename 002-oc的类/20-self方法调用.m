
#import <Foundation/Foundation.h>

@interface Dog : NSObject {
    
    int _speed;
    
}

- (void) setSpeed:(int) speed;
- (int) speed;

- (void)bark;
- (void)run;


@end


@implementation Dog

- (void) setSpeed:(int) speed {
    
    _speed = speed;
    
}

- (int) speed {
    
    return _speed;
}

- (void)bark {
    
    NSLog(@"速度为%i的狗在汪汪", _speed);
    
}
- (void)run {
    
    // 给self指向的对象发送一条bark消息
    [self bark];
//    NSLog(@"速度为%i的狗在汪汪", _speed);
    
    NSLog(@"速度为%i的狗在跑", _speed);

}


@end




int main() {
    
    Dog *dog = [Dog new];
    [dog setSpeed:250];
    [dog run];
    
    
    return 0;
}










