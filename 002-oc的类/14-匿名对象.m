#import <Foundation/Foundation.h>


@interface Car : NSObject {

    @public
    int speed;
}

- (void) fly;

@end

@implementation Car

- (void) fly {
    
    NSLog(@"某个车子飞起来了,车速是%i",speed);
}

@end

int main() {
    
    // 没有变量名  匿名对象(内存泄露)
    [Car new]->speed = 100;
    [[Car new] fly];
    
    
    return 0;
}

