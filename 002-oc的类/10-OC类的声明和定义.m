#import <Foundation/Foundation.h>

// 僵尸类的声明
@interface Zoombie : NSObject {
    
    // 这些属性成为对象的"成员变量"
    
    @public // @public后面的所有属性都可以被外界访问
    int life; // 生命值
    int speed; // 速度
    
}
// 声明方法
- (void)walk;

@end

@implementation Zoombie

- (void)walk {
    NSLog(@"生命值为%i的僵尸在走!!!", life);
}

@end


int main() {
    
    Zoombie *zoom = [Zoombie new];
    zoom->life = 100;
    
    [zoom walk];
    
    return 0;
}

