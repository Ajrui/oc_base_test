
#import <Foundation/Foundation.h>

// 定义一个类
@implementation Car : NSObject
// 该大括号只是用来定义属性
{
    @public
    int wheels;
    double speed;
    
}

// 定义方法 - (void) 方法名
- (void) run
{
    NSLog(@"%i个轮子 %f的时速跑起来了", wheels,speed);
}

- (void) stop
{
    NSLog(@"汽车停下来了");
}


@end

int main()
{
    Car *c = [Car new];
    
    c->wheels = 4;
    c->speed = 120;
    
    
    [c run];
    [c stop];
    
    
}

