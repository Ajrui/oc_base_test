
#import <Foundation/Foundation.h>

// 声明
@interface Car : NSObject {
    
//    @public
    int wheels; // 轮子个数
    
}

/*
 set方法
 1.作用:用来设置成员变量,可以在方法里面过滤掉一些不合理的值
 2.命令规范:
    1>方法都是set开头,而且后面跟上成员变量名,成员变量名的首字母大写.
    2>形参名称不要跟成员变量同名。
 */
- (void) setWheels:(int)newWheels;

/*
 get方法
 1.作用:返回对象内部的成员变量
 2.命名规范:
    1>get方法的名称一般就跟成员变量同名
 */
- (int) wheels;


// 跑
- (void) run;

@end

@implementation Car

// 跑的实现
- (void) run {
    
    NSLog(@"%i个轮子的车跑起来了!",wheels);
}

// set方法的实现
- (void) setWheels:(int)newWheels {
    
    // 对外面传进来的轮子数进行过滤
    if(newWheels <= 0) {
        newWheels = 1;
    }
    
    wheels = newWheels;
}

// get方法的实现
- (int) wheels {
    
    return wheels;
}

@end


int main() {
    
    Car *car = [Car new];
//    car->wheels = 4;
    [car setWheels:4];
    
    [car run];
    
    NSLog(@"轮子的个数是%i",[car wheels]);
    
    return 0;
    
}