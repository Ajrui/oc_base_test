#import <Foundation/Foundation.h>


@implementation Bird : NSObject
{
    @public
    int height; // 飞的高度
}

- (void) fly
{
    NSLog(@"鸟儿飞了%i这么高!", height);
}



@end

// 指针只能改变它指向的对象

void test(Bird *myb)
{
//    myb->height = 500;
    Bird *b2 = [Bird new];
    b2->height = 500;
    myb = b2;
    
}


int main()
{
    Bird *b = [Bird new];
    b->height = 1000;
    
    test(b);
    [b fly]; // 打印1000
    
    return 0;
}



