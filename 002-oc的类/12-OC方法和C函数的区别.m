
/*
 OC对象方法和函数的区别
 1.函数属于整个文件,在文件的任意地方都能调用;对象方法只属于对象,只有对象才能调用对象方法
 2.对象方法只能声明在@interface和@end之间,对象方法只能实现在@implementation和@end之间
   函数的声明和定义能写在任意地方,函数不能归某个类所有,只属于某个文件
 */
#import <Foundation/Foundation.h>

// 车的声明
@interface Car : NSObject {
    
    // 成员变量(实例变量)
    @public
    int speed;
}

- (void) run;

@end

// 车的实现
@implementation Car

- (void) run {
    
    NSLog(@"速度为%i的车子跑起来了！",speed);
}

// 该函数跟类、对象一点关系都没有
void show() {
    
    NSLog(@"调用了show方法");
    
}

@end

void test() {
    
    NSLog(@"调用了test方法");
    
}

int main() {
    
    Car *c = [Car new];
    c->speed = 100;
    [c run];
    
    test();
    show();
    
    return 0;
    
}