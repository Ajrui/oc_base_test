#import <Foundation/Foundation.h>

@interface Dog : NSObject {
    
    @public
    int speed;
}

- (void) run;

@end

@implementation Dog

- (void) run {
    
    NSLog(@"速度是%i的狗跑起来了~~",speed);
}

@end


int main() {
    
    Dog *dog = [Dog new];
    dog->speed = 300;
    
    [dog run];
    
    return 0;
    
    
}
