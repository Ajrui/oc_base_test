//
//  main.m
//  06-Xcode的功能
//
//  Created by 何志勇 on 15/9/9.
//  Copyright (c) 2015年 Jason. All rights reserved.
//  Command + R

#import <Foundation/Foundation.h>
#import "Dog.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Dog *dog = [[Dog alloc] init];
        
        dog.age = 20;
        
        int a = dog.age;
        
        NSLog(@"Hello, World!  %d",a);
    }
    return 0;
}
