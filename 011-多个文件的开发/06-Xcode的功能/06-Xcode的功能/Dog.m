//
//  Dog.m
//  06-Xcode的功能
//
//  Created by 何志勇 on 15/9/9.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Dog.h"

@implementation Dog

- (void) test1 {
    
}

- (void) test2{
    
}

- (void) test3 {
    
}

#pragma mark - 这是一组方法
- (void) test4 {
    
}

- (void) test5 {
    
}

- (void) test6 {
    
}

- (void)setAge:(int)age {
    
    _age = age;
}

#pragma mark - 这是一组方法
#pragma mark 这个是age的get方法
- (int)age {
    
    return _age;
}
@end
