//
//  Dog.h
//  06-Xcode的功能
//
//  Created by 何志勇 on 15/9/9.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dog : NSObject {
    
    int _age;
}
//@property int age;

- (void) setAge:(int)age;
- (int) age;
@end
