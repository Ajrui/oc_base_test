//
//  Dog.m
//  05-第一个xcode项目
//
//  Created by 何志勇 on 15/9/9.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Dog.h"

@implementation Dog

- (NSString *)description
{
    return [NSString stringWithFormat:@"speed=%i", _speed];
}

@end
