//
//  main.m
//  05-第一个xcode项目
//
//  Created by 何志勇 on 15/9/9.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dog.h"

int main() {
   
    NSLog(@"Hello, World!");
    
    Dog *dog = [[Dog alloc] init];
    
    dog.speed = 200;
    
//    NSLog(@"speed=%d",dog.speed);

    NSLog(@"%@",dog);
    
    return 0;
}
