
// #import作用跟#include一致,防止头文件的内容被包含多次
// 包含Foundation框架里面的NSObjCRuntime.h
//#import <Foundation/NSObjCRuntime.h>

#import <Foundation/Foundation.h>

int main() {
    
    // 只接受OC字符串,OC字符串以@开头
    NSLog(@"Hello World!");
    
    
    return 0;
}

/*
 1.编写OC程序: .m源文件
 2.编写.m文件为.o目标文件: cc -c xxx.m
 3.链接.o文件为a.out可执行文件: cc xxx.o -framework Foundation
 4.执行a.out文件: ./a.out


*/