
/*
 1.直接输出某个对象的地址
    Person *p= [Person new];
    NSLog(@"%p",p);
 
 2.输出指针变量的地址
    NSLog(@"%p", &p);
 
 3.直接输出整个对象
    NSLog(@"%@",p);
    默认输出是<类名: 对象的内存地址>
    改变默认输出的方式:重写-description方法,返回你想输出的内容
 
 */

#import <Foundation/Foundation.h>

// 声明
@interface Person : NSObject {
    
    int _age;
    @public
    int _height;
}

- (void) setAge :(int)newAge;

- (int) age;


@end

// 实现
@implementation Person

- (void)setAge:(int)newAge {
    
    _age = newAge;
}

- (int) age {
    
    return _age;
}

- (NSString *) description {
    
    
    // 下面这行代码会引发死循环
    // NSLog(@"%@",self);
    
    return [NSString stringWithFormat:@"[age=%d, height=%d]",_age, _height];
}

@end

int main() {
    
    Person *p = [Person new];
    
    [p setAge:28];
    p->_height = 170;
    
    
    NSLog(@"%@",p);
    
    return 0;
}









