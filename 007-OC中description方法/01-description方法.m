#import <Foundation/Foundation.h>

// Person的声明
@interface Person : NSObject {
    
    int _age;
}

- (void) setAge:(int) age;

- (int) age;

@end

// Person的实现
@implementation Person

- (void) setAge:(int)age {
    
    _age = age;
}

- (int) age {
    
    return _age;
}

- (NSString *) description {
    
    
    
    return [NSString stringWithFormat:@"age=%d",_age];
}

@end

int main() {
    
    Person *p = [Person new];
    [p setAge:20];
    
    Person *p2 = p;
    [p2 setAge:30];
    
    // 指针变量p的地址
    NSLog(@"这个Person对象的地址: %p", &p);
    // 对象的地址
    NSLog(@"这个Person对象的地址: %p", p);
    
    NSLog(@"这个Person对象的地址: %p", p2);
    
    NSLog(@"这个Person对象的地址: %d", [p age]);
    
    // 输出所有的OC对象都用%@
    // 默认情况下对象的输出信息: Person对象:<Person: 0x7f83d3c11660>
    // 类名 + 对象的内存地址
    
    // 给指针变量p所指向的对象发送一条-description消息
    // 会调用对象的-description方法,并且把-description方法返回的OC字符串输出到屏幕上
    NSLog(@"Person对象:%@",p);
    
    
    
    return 0;
}