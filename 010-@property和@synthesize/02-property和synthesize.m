#import <Foundation/Foundation.h>


@interface Car : NSObject {
    
    int _wheels;
//    int _speed;
}

@property int wheels;
@property int speed;

@end

@implementation Car
// 默认会访问wheels成员变量,如果这个成员变量不存在,自动生成一个私有的wheels变量
@synthesize wheels;

// setter和getter会访问_speed成员变量,如果这个成员变量不存在,会自动生成一个私有的_speed变量
@synthesize speed = _speed;

- (NSString *) description {
    
    return [NSString stringWithFormat:@"wheels=%d, _speed=%d", wheels,_speed];
}

@end

int main() {
    
    Car *c = [[Car alloc] init];
    
    c.wheels = 4;
    
    c.speed = 250;
    
    NSLog(@"轮子个数: %d",c.wheels);
    
    NSLog(@"%@",c);
    
    
    return 0;
}