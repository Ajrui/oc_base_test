#import <Foundation/Foundation.h>

/*
 总结：
    @property
        1.只能用在@interface和@end之间
        2.作用:
            1>Xcode 4.4前:自动生成set方法和get方法的声明
            2>Xcode 4.4后:自动生成set方法和get方法的声明和实现、增加一个_开头的成员变量
 
 
    @synthesize
        1.只能用在@implementation和@end之间
        2.作用: 自动生成set方法和get方法的实现
 
 */

@interface Dog : NSObject
//{
//    int _speed;
//}

@property int speed; // 现在一个property可以生成以下注释掉的代码

@end

@implementation Dog
//@synthesize speed = _speed;

/*
{
    int _speed;
}

- (void) setSpeed:(int)speed
{
    _speed = speed;
}

- (int) speed
{
    return _speed;
}
*/


- (void) setSpeed:(int)speed {
    // 如果要在set方法里面做操作 就必须得重写该方法
    if(speed < 0) {
        
        speed = 1;
    }
    
    _speed = speed;
}

// 如果同时实现了set和get方法  那么它不会自己实现成员变量了 故报错
/*
- (int) speed {
    
    return _speed;
}
 */

- (NSString *) description {
    
    return  [NSString stringWithFormat:@"_speed=%d", _speed];
}

@end



int main() {
    
    Dog *dog = [[Dog alloc] init];
    dog.speed = -200;
    
    NSLog(@"%@",dog);
    
    return 0;
}
