/*
 @property
    1.用在@interface中
    2.用来自动生成setter和getter的声明
 */

#import <Foundation/Foundation.h>


// 声明
@interface Person : NSObject {
    
    int _age;
    
    int _height;
    
    int _weight;
    
    double _money;
    
    double money;
    
}

//- (void) setAge:(int)newAge;
//- (int)age;
// @property会自动生成get方法和set方法的声明
@property int age;



//- (void)setHeight:(int)newHeight;
//- (int) height;
@property int height;

@property double money;

- (void) test;

@end

// 实现
@implementation Person

- (void) test {
    
    NSLog(@"money=%f, _money=%f", money, _money);
}


// 自动生成setter和getter的实现
// 在实现中访问_age这个成员变量
@synthesize age = _age;

/*
- (void) setAge:(int)newAge {
    
    _age = newAge;
}

- (int) age {
    
    return _age;
}
 */

@synthesize height = _height;

// 如果没有明确指定成员变量名,实现中默认访问的就是同名的成员变量money
@synthesize money = _money;

/*
- (void) setHeight:(int)newHeight {
    
    _height = newHeight;
}

- (int) height {
    
    return _height;
}
 */

@end





int main() {
    
    Person *p = [[Person alloc] init];
//    p.age = 200;
//    [p setAge:20];
//    
//    NSLog(@"人的年龄是%d",[p age]);
    
    p.money = 100;
    
    [p test];
    
    return 0;
}



