#import <Foundation/Foundation.h>

@interface Zoombie : NSObject

- (void) walk;

+ (void) walk;

@end

@implementation Zoombie

- (void) walk {
    
    NSLog(@"往前挪几步...");
}

+ (void) walk {
    
    NSLog(@"----------");
}


@end

@interface DancerZoombie : Zoombie
@end

@implementation DancerZoombie

- (void) walk {
    
    // 调用父类中实现的walk方法
    // 调用的对象方法还是类方法, 取决于super所在的环境
    [super walk]; // 取决于super所在的环境 该方法是在对象方法中 因此调用父类的对象方法
    
    // 如果子类需要在父类方法的基础上进行重写,,应该用super调用父类的方法;
    NSLog(@"跳几下");
}

@end
          
int main () {
    
    DancerZoombie *dancer = [DancerZoombie new];
    
    [dancer walk];
    
    return 0;
              
}
