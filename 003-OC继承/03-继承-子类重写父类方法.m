
/*
 继承要点:
    1.当调用方法时,首先检测子类有没有实现这个方法,如果没有实现,就会调用父类的实现
    2.重写:子类实现父类中声明的方法
 */


#import <Foundation/Foundation.h>

@interface Person : NSObject {
    int _age;
}

- (void) study;

+ (void) test;

@end


@implementation Person

- (void) study {
    
    NSLog(@"某人正在学习中.....  Person");
}

+ (void) test {
    NSLog(@"调用了test方法-----Person");
}

@end

@interface Student : Person {
    
    int _height;
    int _weight;
}

- (void) fight;

@end

@implementation Student

- (void) fight {
    
    NSLog(@"调用了fight方法");
}

- (void) study {
    
    NSLog(@"某学生正在学习......Student");
}

@end

int main() {
    
    Student *student = [Student new];
    [student study];
    
    [Student test];
    
    return 0;
}
