#import <Foundation/Foundation.h>

// 声明的父类
@interface Animal : NSObject {
    
    int _weight;
}

- (void) setWeight:(int)weight;
- (int) weight;

- (void) eat;

@end

// 实现的父类
@implementation Animal

- (void) setWeight:(int)weight {
    
    _weight = weight;
}

- (int)weight {
    
    return _weight;
}

- (void) eat {
    
    NSLog(@"吃东西的方法被调用,体重是%d的动物",_weight);
}

@end


// 声明子类
@interface Bird : Animal
@end
// 子类的实现
@implementation Bird
@end


// 声明子类
@interface Dog : Animal
@end
// 子类的实现
@implementation Dog
@end




int main() {
    
    Dog *dog = [Dog new];
    [dog setWeight:40];
    [dog eat];
    
    Bird *bird = [Bird new];
    [bird setWeight:5];
    [bird eat];
    
    
    return 0;
}