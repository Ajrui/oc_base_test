#import <Foundation/Foundation.h>

/*
 继承的好处: 抽取了公共代码,提高了代码复用率
 继承的坏处: 代码的耦合性强
 */

// 抽取一个公共类
@interface Animal : NSObject {
    
    int _weight;
}

- (void) setWeight:(int)weight;
- (int) weight;

- (void) eat;
@end

@implementation Animal

- (void) setWeight:(int)weight {
    _weight = weight;
}

- (int) weight {
    
    return _weight;
}

- (void) eat {
    NSLog(@"吃吃吃 -体重: %d", _weight);
}

@end

// Bird的声明
// 这里是冒号 : 代表继承
// Bird继承了Animal、就能拥有Animal的所有方法和成员变量
// Animal是Bird的父类 (超类 superclass)
@interface Bird : Animal
@end
// Bird的实现
@implementation Bird
@end

// Dog的声明
// Dog继承了Animal，就能拥有Animal的所有方法和成员变量
// Animal是Dog的父类（超类 superclass）
@interface Dog : Animal
@end
// Dog的实现
@implementation Dog
@end

// Bird、Dog都是Animal的子类（subclasses）

int main()
{
    Dog *dog = [Dog new];
    [dog setWeight:100];
    [dog eat];
    
    return 0;
}









