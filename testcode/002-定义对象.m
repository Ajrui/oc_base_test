
#import <Foundation/Foundation.h>

@interface Person : NSObject {
    
    int _age;
    
}

- (void) setAge:(int)age;

- (int)age;

- (void) print;

@end


@implementation Person

- (void) setAge:(int)age {
    
    _age = age;
}

- (int) age {
    
    return _age;
}

- (void) print {
    
    NSLog(@"该人的年龄是%i", _age);
}

@end

int main() {
    
    Person *p = [Person new];
    [p setAge:20];
    
    [p print];
    
    return 0;
    
}




