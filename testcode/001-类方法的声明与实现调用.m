#import <Foundation/Foundation.h>

// 声明
@interface Caculator : NSObject

+ (double)pi;

+ (double)pi:(int) abc;

+ (double)pingfang:(double) number;

+ (double)sumOfNum1:(double) num1 andNum2:(double)num2;

@end

// 实现
@implementation Caculator

+ (double)pi {
    
    return 3.14;
}

+ (double)pi:(int)abc {
    
    return abc + [self pi];
}

+ (double) pingfang:(double)number {
    
    return number * number;
}

+ (double) sumOfNum1:(double)num1 andNum2:(double)num2 {
    
    return num1 + num2;
}


@end


int main() {
    
    
    NSLog(@"pi无参数的返回值是%f", [Caculator pi]);
    NSLog(@"pi带参数的返回值是%f", [Caculator pi:4]);
    NSLog(@"pingfang的结果是%f", [Caculator pingfang:4]);
    NSLog(@"sumOfNum1的和是%f", [Caculator sumOfNum1:4 andNum2:6]);
    
    
    return 0;
}





