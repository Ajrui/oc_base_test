#import <Foundation/Foundation.h>


@interface Person : NSObject {
    
    int _age;
}

- (void) setAge:(int)newAge;

- (int) age;

@end

@implementation Person

- (void) setAge:(int)newAge {
    
    _age = newAge;
}

- (int) age {
    
    return _age;
}

- (id) init {
    
    
    if (self = [super init]) {
        
         _age = 20;
    }
    
    
    return self;
}

@end

@interface Student : Person {
    
    int _no;
}

- (void) setNo:(int)newNo ;
- (int) no ;

@end

@implementation Student

- (void) setNo:(int)newNo  {
    
    _no = newNo;
}
- (int) no {
    
    return _no;
}

- (id) init {
    
//    _age = 20;
   
    
    if(self = [super init]) {
        _no = 5;
    }
    
    return self;
    
}

@end


int main() {
    
    // age = 20 no = 5;
    Student *stu = [[Student alloc] init];
    
    NSLog(@"age=%i, no=%i", [stu age], [stu no]);
    
    return 0;
}


