#import <Foundation/Foundation.h>

// 声明
@interface Person : NSObject {
    
    int _age;
}

- (void)setAge:(int)newAge;
- (int) age;

// 自定义构造方法
/*
 规范:
    1.返回值是id类型
    2.方法名都是以init开头
 */

- (id) initWithAge:(int)age;

@end

@implementation Person

- (void) setAge:(int)newAge {
    
    _age = newAge;
}

- (int) age {
    
    return _age;
}

// 自定义方法的实现
- (id) initWithAge:(int) age {
    
    if (self = [super init]) {
        
        _age = age;
    }
    
    return self;
}

@end


int main () {
    
    Person *p = [[Person alloc] initWithAge:28];
    
    NSLog(@"age=%d",[p age]);
    
    return 0;
}


