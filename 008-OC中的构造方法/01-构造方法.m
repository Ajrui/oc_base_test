#import <Foundation/Foundation.h>

// 声明
@interface Person : NSObject {
    
    int _age;
}

- (void) setAge:(int)newAge;

- (int) age;

@end

@implementation Person

- (void) setAge:(int)newAge {
    
    _age = newAge;
}

- (int) age {
    
    return _age;
}

// id是万能指针,不要再加*
// id类型是能指向任何OC对象
// init方法称为构造方法,构造方法其实是用来初始化对象的
- (id) init {
    
    /*
    // 为了让父类中的成员变量也能初始化
    self = [super init];
    
    if (self != nil) {
        // 说明父类初始化成功
        _age = 20;
    }
     
     */
    
    // 简化写法
    if (self = [super init]) {
        _age = 20;
    }
    
    return self;
    
    
    
}


@end


int main() {
    
    [Person new];
    /*
     new 方法内部做了什么事情
        1.分配内存给对象 +alloc
        2.初始化对象    -init
     */
    
    /*
    // 返回一个已经分配好内存的对象,但是整个对象没有经过初始化
    Person *p = [Person alloc];
    
    // 给指针变量p指向的对象进行初始化操作
    p = [p init];
    
     */
    
    // 返回一个分配好内存、已经初始化好的对象
    Person *p = [[Person alloc] init];
    
    NSLog(@"age=%d",[p age]);
    
    
    return 0;
}





