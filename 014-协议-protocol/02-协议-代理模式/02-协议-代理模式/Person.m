//
//  Person.m
//  02-协议-代理模式
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Person.h"

@implementation Person

// 买电影票
- (void)buyTicket {
    
    // 叫代理去帮自己买票
    double price = [_delegate ticketPrice];
    int number = [_delegate leftTicketsNumber];
    
    NSLog(@"通过代理的帮忙,票价=%f, 还剩%d张票", price, number);
    
}

- (void)dealloc {
    
    [_delegate release];
    [super dealloc];
}

@end
