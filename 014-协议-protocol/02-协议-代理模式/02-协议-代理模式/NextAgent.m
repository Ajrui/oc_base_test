//
//  NextAgent.m
//  02-协议-代理模式
//
//  Created by Jason on 15/9/17.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "NextAgent.h"

@implementation NextAgent

- (int)leftTicketsNumber {
    
    return 200;
}

- (double)ticketPrice {
    
    return 30;
}

@end
