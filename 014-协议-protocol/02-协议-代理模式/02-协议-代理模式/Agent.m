//
//  Agent.m
//  02-协议-代理模式
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import "Agent.h"

@implementation Agent

// 剩余票数
- (int)leftTicketsNumber {
    
    // .. 亲自跑电影院\或者打电话
    
    return 1;
    
}

// 没一张票多少钱
- (double)ticketPrice {
    
    // ... 亲自跑电影院\或者打电话
    
    return 100;
}


@end
