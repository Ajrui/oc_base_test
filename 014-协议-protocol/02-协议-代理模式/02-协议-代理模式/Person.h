//
//  Person.h
//  02-协议-代理模式
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TicketDelegate.h"

@interface Person : NSObject

- (void) buyTicket;


// 拥有一个代理属性
// id代表代理的类名随便 (id 万能指针)
// 但必须遵守TicketDelegate协议
@property (nonatomic, retain) id <TicketDelegate> delegate;

@end
