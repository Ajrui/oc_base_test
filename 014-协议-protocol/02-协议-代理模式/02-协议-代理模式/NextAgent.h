//
//  NextAgent.h
//  02-协议-代理模式
//
//  Created by Jason on 15/9/17.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TicketDelegate.h"

@interface NextAgent : NSObject <TicketDelegate>

@end
