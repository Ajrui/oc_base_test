//
//  TicketDelegate.h
//  02-协议-代理模式
//
//  Created by Jason on 15/9/17.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>

// 声明一些跑腿的方法
@protocol TicketDelegate <NSObject>

// 返回票价
- (double) ticketPrice;

// 还剩下多少张票
- (int) leftTicketsNumber;


@end
