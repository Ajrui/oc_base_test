//
//  MyProtocol.h
//  01-协议-protocol
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

/*
 协议只是用来声明方法
 */

#import <Foundation/Foundation.h>

// 声明一系列方法 (类似java interface接口)  NSObject 是最基协议
@protocol MyProtocol <NSObject>


- (void) test4; // 默认是@required

@required // test1必须实现的
- (void) test1;

@optional // test2、test3是可选实现的
- (void) test2;
- (void) test3;


@end
