//
//  Person.h
//  01-协议-protocol
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProtocol.h"
#import "MyProtocol2.h"
#import "MyProcotol3.h"

// : 继承
// <> 遵守某个协议,只要遵守了这个协议,相当于拥有协议里面的所有方法声明
// 编译器不强求实现协议里所有的方法
// 可以遵守多个协议
@interface Person : NSObject <MyProtocol,MyProcotol3>

@end
