//
//  MyProtocol2.h
//  01-协议-protocol
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MyProtocol2 <NSObject>

- (void) haha;

@optional
- (void) hehe;

@end
