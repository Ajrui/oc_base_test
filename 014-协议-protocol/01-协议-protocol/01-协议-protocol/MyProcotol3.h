//
//  MyProcoto3.h
//  01-协议-protocol
//
//  Created by Jason on 15/9/16.
//  Copyright (c) 2015年 Jason. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProtocol2.h"

// 一个协议可以遵守其他协议
@protocol MyProcotol3 <MyProtocol2>

- (void) hiahia;

@end
