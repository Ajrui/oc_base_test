#import <Foundation/Foundation.h>

@interface Person : NSObject {
    
    int _age;
}

- (void) setAge:(int)newAge;

- (int) age;

@end



@implementation Person

- (void) setAge:(int)newAge {
    // 下面代码  会死循环
//    self.age = newAge; // [self setAge:newAge];
    
    _age = newAge;
}

- (int) age {
    
//    return self.age; // [self age]; // 也是死循环
    
    return _age;
}

@end

int main() {
    
    Person *p = [[Person alloc] init];
    
//    [p setAge:19];
    
    // “.”并不是访问成员变量
    p.age = 19; // 编译器会自动生成右边的代码 [p setAge:19];
    
    int newAge = p.age;// 编译器会自动生成右边的代码 int newAge = [p age];
    
    NSLog(@"age=%d", [p age]);
    
    
    return 0;
}